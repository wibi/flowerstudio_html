
<!-- header -->
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper main__container--signup"> <!-- bg--blur -->
                  <div class="main__container__2">
                        <br>
                        <div class="main__signup--img"></div>
                        <div class="main__signup--box">
                              <h2 class="main__signup--h2">LOGIN</h2>
                              <span class="info__alert alert--failed"><span class="icon--alert--failed"></span>Invalid email / password. Please double-check your LOGIN information and try again.</span>
                              <form class="" action="">
                                    <div class="input__side">
                                          <label for="" class="loginpage--label">Email</label>
                                          <input class="loginpage--input" type="text"><br>
                                          <label for="" class="loginpage--label">Password</label>
                                          <input class="loginpage--input" type="text">
                                    </div>
                                    <a href="" class="button--login--popup">LOGIN</a><font class="popup__loginpage--font">or <a href="">Sign Up</a></font><br>
                                    <a href="" class="popup__loginpage--forgot">Forgot my password</a>
                              </form>
                        </div> 
                  </div>               
                  <span class="bg--shadow"></span>                 
		</section>            

<!-- footer -->
<?php include("includes/footer.php"); ?>

