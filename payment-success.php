
<!-- header -->
<?php include("includes/header.php"); ?>

    <!-- main -->
    <section class="main__container__wrapper"> <!-- bg--blur -->
              <div class="main__container__2">
                    <p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / 
                      <a href="" class="breadscrumbs--a"> Shopping Cart</a> / 
                      <a href="" class="breadscrumbs--a"> Shipping</a> /
                      <a href="" class="breadscrumbs--a"> Payment</a>
                    </p>
                                                
                    <!-- shipping wrapper -->
                    <section class="account__wrapper"> 
                              <div class="payment__step--main">
                                    
                                    <!-- success order payment -->
                                    <span class="payment__success">
                                        <h2 class="payment__success--h2">ORDER SUCCESS<span class="icon--paymentmethod--success"></span></h2>
                                        <hr class="payment__success--hr">
                                        <p class="payment__success--p">
                                          Dear customer,<br><br>

                                          Thank you for shopping at Flower Studio. We hope you have enjoyed your shopping with us<br>
                                          and hope you will shop with us again. We have sent a confirmation letter to <a href="mailto:abdcd@email.com">abdcd@email.com</a>
                                        </p>
                                        <br>
                                        <h4 class="payment__success--h4">Your invoice number is #17111401</h4>
                                        <p class="payment__success--p">
                                          <span class="payment__success__label"><b>Total payment</b></span><b>: Rp 21.000.000</b><br>
                                          <span class="payment__success__label"><b>Adjustment code</b></span><b>: Rp 249</b><br>
                                          <span class="payment__success__labelhr"></span><br>
                                          <span class="payment__success__label"><b>Grand Total</b></span><b>: Rp 21.000.249</b><br><br>

                                          <h5 class="payment__success--h5">
                                            <i>* The Adjustment Code is your personal payment code for our administration purpose
                                             to proceed your order faster and easier.</i>
                                          </h5><br>                                         

                                          Please fill out and submit the payment confirmation page which you can find in the “My Account” page.
                                          If you don’t submit your confirmation within 1 x 24 hours from your order time, your order will be expired.
                                        </p><br><br>
                                        <a href="" class="button--order--success">My Order</a>
                                        <a href="" class="button--order--success">Back to shop</a>
                                    </span>
                                    
                                    <!-- success payment -->
                                    <span class="payment__success">
                                        <h2 class="payment__success--h2">SUCCESS<span class="icon--paymentmethod--success"></span></h2>
                                        <hr class="payment__success--hr">
                                        <p class="payment__success--p">
                                          Dear customer,<br><br>

                                          Thank you for shopping at Flower Studio. We hope you have enjoyed your shopping with us<br>
                                          and hope you will shop with us again. We have sent a confirmation letter to <a href="mailto:abdcd@email.com">abdcd@email.com</a>
                                        </p>
                                        <br>
                                        <h4 class="payment__success--h4">Your invoice number is #17111401</h4>
                                        <p class="payment__success--p">
                                          <span class="payment__success__label"><b>Total payment</b></span><b>: Rp 21.000.000</b><br><br>                                        

                                          We will process your order immediately.  Thank you and have a great day.
                                        </p><br><br>
                                        <a href="" class="button--order--success">My Order</a>
                                        <a href="" class="button--order--success">Back to shop</a>
                                    </span>
                                     
                              </div>        
                    </section>
              </div>

              <!-- popup search -->
              <?php include("includes/popup_search.php"); ?>

    </section>

<!-- footer -->
<?php include("includes/footer.php"); ?>

