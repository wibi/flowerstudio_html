/*! normalize.css v1.0.2 | MIT License | git.io/normalize */
/* ==========================================================================
   HTML5 display definitions
   ========================================================================== */
/*
 * Corrects `block` display not defined in IE 6/7/8/9 and Firefox 3.
 */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
nav,
section,
summary {
  display: block;
}

/*
 * Corrects `inline-block` display not defined in IE 6/7/8/9 and Firefox 3.
 */
audio,
canvas,
video {
  display: inline-block;
  *display: inline;
  *zoom: 1;
}

/*
 * Prevents modern browsers from displaying `audio` without controls.
 * Remove excess height in iOS 5 devices.
 */
audio:not([controls]) {
  display: none;
  height: 0;
}

/*
 * Addresses styling for `hidden` attribute not present in IE 7/8/9, Firefox 3,
 * and Safari 4.
 * Known issue: no IE 6 support.
 */
[hidden] {
  display: none;
}

/* ==========================================================================
   Base
   ========================================================================== */
/*
 * 1. Corrects text resizing oddly in IE 6/7 when body `font-size` is set using
 *    `em` units.
 * 2. Prevents iOS text size adjust after orientation change, without disabling
 *    user zoom.
 */
html {
  font-size: 100%;
  /* 1 */
  -webkit-text-size-adjust: 100%;
  /* 2 */
  -ms-text-size-adjust: 100%;
  /* 2 */
}

/*
 * Addresses `font-family` inconsistency between `textarea` and other form
 * elements.
 */
html,
button,
input,
select,
textarea {
  font-family: sans-serif;
}

/*
 * Addresses margins handled incorrectly in IE 6/7.
 */
body {
  margin: 0;
}

/* ==========================================================================
   Links
   ========================================================================== */
/*
 * Addresses `outline` inconsistency between Chrome and other browsers.
 */
/*
 * Improves readability when focused and also mouse hovered in all browsers.
 */
a:focus,
a:active,
a:hover {
  outline: 0;
}

/* ==========================================================================
   Typography
   ========================================================================== */
/*
 * Addresses font sizes and margins set differently in IE 6/7.
 * Addresses font sizes within `section` and `article` in Firefox 4+, Safari 5,
 * and Chrome.
 */
/*
 * Addresses styling not present in IE 7/8/9, Safari 5, and Chrome.
 */
abbr[title] {
  border-bottom: 1px dotted;
}

/*
 * Addresses style set to `bolder` in Firefox 3+, Safari 4/5, and Chrome.
 */
b,
strong {
  font-weight: bold;
}

blockquote {
  margin: 1em 40px;
}

/*
 * Addresses styling not present in Safari 5 and Chrome.
 */
dfn {
  font-style: italic;
}

/*
 * Addresses styling not present in IE 6/7/8/9.
 */
mark {
  background: #ff0;
  color: #000;
}

/*
 * Addresses margins set differently in IE 6/7.
 */
p,
pre {
  margin: 1em 0;
}

/*
 * Corrects font family set oddly in IE 6, Safari 4/5, and Chrome.
 */
code,
kbd,
pre,
samp {
  font-family: monospace, serif;
  _font-family: 'courier new', monospace;
  font-size: 1em;
}

/*
 * Improves readability of pre-formatted text in all browsers.
 */
pre {
  white-space: pre;
  white-space: pre-wrap;
  word-wrap: break-word;
}

/*
 * Addresses CSS quotes not supported in IE 6/7.
 */
q {
  quotes: none;
}

/*
 * Addresses `quotes` property not supported in Safari 4.
 */
q:before,
q:after {
  content: '';
  content: none;
}

/*
 * Addresses inconsistent and variable font size in all browsers.
 */
/*
 * Prevents `sub` and `sup` affecting `line-height` in all browsers.
 */
sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sup {
  top: -0.5em;
}

sub {
  bottom: -0.25em;
}

/* ==========================================================================
   Lists
   ========================================================================== */
/*
 * Addresses margins set differently in IE 6/7.
 */
dl,
menu,
ol,
ul {
  margin: 1em 0;
}

dd {
  margin: 0 0 0 40px;
}

/*
 * Addresses paddings set differently in IE 6/7.
 */
/*
 * Corrects list images handled incorrectly in IE 7.
 */
nav ul,
nav ol {
  list-style: none;
  list-style-image: none;
}

/* ==========================================================================
   Embedded content
   ========================================================================== */
/*
 * 1. Removes border when inside `a` element in IE 6/7/8/9 and Firefox 3.
 * 2. Improves image quality when scaled in IE 7.
 */
img {
  border: 0;
  /* 1 */
  -ms-interpolation-mode: bicubic;
  /* 2 */
}

/*
 * Corrects overflow displayed oddly in IE 9.
 */
svg:not(:root) {
  overflow: hidden;
}

/* ==========================================================================
   Figures
   ========================================================================== */
/*
 * Addresses margin not present in IE 6/7/8/9, Safari 5, and Opera 11.
 */
figure {
  margin: 0;
}

/* ==========================================================================
   Forms
   ========================================================================== */
/*
 * Corrects margin displayed oddly in IE 6/7.
 */
form {
  margin: 0;
}

/*
 * Define consistent border, margin, and padding.
 */
fieldset {
  border: 1px solid #c0c0c0;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em;
}

/*
 * 1. Corrects color not being inherited in IE 6/7/8/9.
 * 2. Corrects text not wrapping in Firefox 3.
 * 3. Corrects alignment displayed oddly in IE 6/7.
 */
legend {
  border: 0;
  /* 1 */
  padding: 0;
  white-space: normal;
  /* 2 */
  *margin-left: -7px;
  /* 3 */
}

/*
 * 1. Corrects font size not being inherited in all browsers.
 * 2. Addresses margins set differently in IE 6/7, Firefox 3+, Safari 5,
 *    and Chrome.
 * 3. Improves appearance and consistency in all browsers.
 */
button,
input,
select,
textarea {
  font-size: 100%;
  /* 1 */
  margin: 0;
  /* 2 */
  vertical-align: baseline;
  /* 3 */
  *vertical-align: middle;
  /* 3 */
}

/*
 * Addresses Firefox 3+ setting `line-height` on `input` using `!important` in
 * the UA stylesheet.
 */
button,
input {
  line-height: normal;
}

/*
 * 1. Avoid the WebKit bug in Android 4.0.* where (2) destroys native `audio`
 *    and `video` controls.
 * 2. Corrects inability to style clickable `input` types in iOS.
 * 3. Improves usability and consistency of cursor style between image-type
 *    `input` and others.
 * 4. Removes inner spacing in IE 7 without affecting normal text inputs.
 *    Known issue: inner spacing remains in IE 6.
 */
button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  /* 2 */
  cursor: pointer;
  /* 3 */
  *overflow: visible;
  /* 4 */
}

/*
 * Re-set default cursor for disabled elements.
 */
button[disabled],
input[disabled] {
  cursor: default;
}

/*
 * 1. Addresses box sizing set to content-box in IE 8/9.
 * 2. Removes excess padding in IE 8/9.
 * 3. Removes excess padding in IE 7.
 *    Known issue: excess padding remains in IE 6.
 */
input[type="checkbox"],
input[type="radio"] {
  box-sizing: border-box;
  /* 1 */
  padding: 0;
  /* 2 */
  *height: 13px;
  /* 3 */
  *width: 13px;
  /* 3 */
}

/*
 * 1. Addresses `appearance` set to `searchfield` in Safari 5 and Chrome.
 * 2. Addresses `box-sizing` set to `border-box` in Safari 5 and Chrome
 *    (include `-moz` to future-proof).
 */
input[type="search"] {
  -webkit-appearance: textfield;
  /* 1 */
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  /* 2 */
  box-sizing: content-box;
}

/*
 * Removes inner padding and search cancel button in Safari 5 and Chrome
 * on OS X.
 */
input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}

/*
 * Removes inner padding and border in Firefox 3+.
 */
button::-moz-focus-inner,
input::-moz-focus-inner {
  border: 0;
  padding: 0;
}

/*
 * 1. Removes default vertical scrollbar in IE 6/7/8/9.
 * 2. Improves readability and alignment in all browsers.
 */
textarea {
  overflow: auto;
  /* 1 */
  vertical-align: top;
  /* 2 */
}

/* ==========================================================================
   Tables
   ========================================================================== */
/*
 * Remove most spacing between table cells.
 */
table {
  border-collapse: collapse;
  border-spacing: 0;
}

h1 {
  font-size: 36px;
  font-weight: normal;
  margin: 0 0 0 0;
  padding: 0 0 0 0;
}

h2 {
  font-size: 20px;
  font-weight: normal;
  margin: 0 0 0 0;
  padding: 0 0 0 0;
}

h3 {
  font-size: 18px;
  font-weight: bold;
  margin: 0 0 0 0;
  padding: 0 0 0 0;
}

h4 {
  font-size: 16px;
  font-weight: bold;
  margin: 0 0 0 0;
  padding: 0 0 0 0;
}

h5 {
  font-size: 14px;
  font-weight: normal;
  margin: 0 0 0 0;
  padding: 0 0 0 0;
}

a.link, .link {
  text-decoration: none;
}

a {
  text-decoration: none;
}

article p {
  font-size: 16px;
  line-height: 22px;
}

p {
  font-size: 16px;
}
p.p--small {
  color: #848c94;
  font-size: 13px;
  line-height: 18px;
  margin: 3px 0;
}

blockquote {
  font-style: italic;
  color: white;
  font-size: 22px;
  margin: 20px 0;
  position: relative;
}
blockquote:before {
  content: '';
  width: 38px;
  height: 24px;
  position: absolute;
  left: -40px;
  top: 0;
  background: url(../images/sprite.png) -150px 0 no-repeat;
}

small {
  font-size: 12px;
}

section.header {
  display: inline-block;
  width: 100%;
}
section.header .header__menu__wrapper .header__menu--logo {
  float: left;
}
section.header .header__menu__wrapper .header__menu--menu {
  float: right;
  width: 820px;
  margin: 0;
  padding: 17px 0;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account {
  clear: both;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account .menu__account--ul {
  display: inline-block;
  width: 167px;
  float: right;
  margin: 0 10px 3px 0;
  padding: 0;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account .menu__account--ul .menu__account--li {
  display: inline-block;
  margin: 5px 8px;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account .menu__account--ul .menu__account--li .icons__cart {
  position: relative;
  cursor: pointer;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account .menu__account--ul .menu__account--li .icons__cart .cart--nmr {
  position: absolute;
  top: -11px;
  right: -12px;
  margin: 0;
  padding: 0;
  width: 19px;
  height: 19px;
  background: #176efb;
  border-radius: 50%;
  color: #fff;
  font-size: 11px;
  text-align: center;
  line-height: 19px;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__account .menu__account--ul .menu__account--li .button--login {
  margin: -4px 0 0 0;
}
section.header .header__menu__wrapper .header__menu--menu hr {
  clear: both;
  border-top: 1px solid #000;
  border-right: 0;
  border-bottom: 0;
  border-left: 0;
  margin: 0;
  padding: 0;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__page {
  clear: both;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__page .menu__page--ul {
  display: inline-block;
  width: 535px;
  float: right;
  margin: 0;
  padding: 0;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__page .menu__page--ul .menu__page--li {
  display: inline-block;
  float: left;
  text-align: right;
  margin: 5px 16px;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__page .menu__page--ul .menu__page--li a {
  font-size: 16px;
  color: #000;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__page .menu__page--ul .menu__page--li a:hover {
  color: #b49b8a;
}
section.header .header__menu__wrapper .header__menu--menu .logo--menu {
  float: right;
  cursor: pointer;
  margin: 10px 10px 0 0;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__responsive {
  position: absolute;
  top: 85px;
  right: 10px;
  z-index: 100;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__responsive .menu__responsive--ul {
  display: inline-block;
  margin: 0;
  padding: 0;
  background: #5e5d5d;
  width: 170px;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__responsive .menu__responsive--ul .menu__responsive--li {
  width: 170px;
  text-align: left;
  padding: 8px 0px;
  border-bottom: 1px solid #f1f1f1;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__responsive .menu__responsive--ul .menu__responsive--li:hover {
  background: #1f72fc;
}
section.header .header__menu__wrapper .header__menu--menu nav.menu__responsive .menu__responsive--ul .menu__responsive--li a {
  display: block;
  font-size: 14px;
  font-weight: 100;
  letter-spacing: 1px;
  color: #fff;
  padding: 5px 0px 5px 10px;
}

/** responsive **/
/* (0px above)----------- */
@media only screen and (min-width: 0px) {
  section.header {
    height: 100px;
  }

  .header__menu--logo {
    -moz-transition: scale(0.8);
    -webkit-transition: scale(0.8);
    -o-transform: scale(0.8);
    transform: scale(0.8);
  }

  .header__menu--logo {
    position: absolute;
    top: 0;
    left: -13px;
  }

  .cart--search,
  .button--login,
  .menu__page {
    display: none;
  }

  .menu__account--li {
    float: right;
  }
}
/* (768px above)----------- */
@media only screen and (min-width: 768px) {
  .cart--search,
  .button--login,
  .menu__page {
    display: block;
  }

  .menu__account--li {
    float: left;
  }

  .logo--menu,
  nav.menu__responsive {
    display: none;
  }
}
/* (1024px above)----------- */
@media only screen and (min-width: 1024px) {
  section.header {
    height: auto;
  }

  .header__menu__wrapper {
    width: 960px;
    margin: 20px auto;
  }

  .header__menu--logo {
    -moz-transition: scale(1);
    -webkit-transition: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }

  .header__menu--logo {
    position: relative;
  }
}
/* (1360px above)----------- */
/* (1440px above)----------- */
.button--login {
  font-size: 12px;
  color: #fff;
  padding: 7px 15px;
  background: #555;
  border-radius: 5px;
}

/* icons */
.logo--flowerstudio {
  width: 79px;
  height: 93px;
  padding: 0px 30px 0px 20px;
  background: url("../images/logo--flowerstudio.png") no-repeat center center white;
}

.logo--menu {
  width: 28px;
  height: 18px;
  background: url("../images/logo--menu.png") no-repeat;
}

.icons__cart {
  width: 23px;
  height: 23px;
}

.cart--cart {
  background: url(../images/logo--cart--mini.png) no-repeat;
  margin-right: 5px;
}

.cart--search {
  background: url(../images/logo--search--mini.png) no-repeat;
}

.icon--article {
  float: left;
  display: inline-block;
  width: 25px;
  height: 22px;
}

.logo--phone {
  background: url(../images/logo--phone.png) no-repeat;
}

.logo--email {
  background: url(../images/logo--email.png) no-repeat;
}
