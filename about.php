
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper">
			<center>
				<img class="molt banner--article" 
				data-molt-0w="assets/images/img__banner__page/banner--aboutus--320.jpg"
		  		data-molt-480w="assets/images/img__banner__page/banner--aboutus--768.jpg"
		  		data-molt-768w="assets/images/img__banner__page/banner--aboutus--1360.jpg" 
		  		data-molt-1400w="assets/images/img__banner__page/banner--aboutus--1920.jpg" alt="">
			</center>
            <div class="main__container" style="display:block !important;">
            	<p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> About Us</a></p>
            	<h1>About Us</h1>
            	<article class="main__container--article">            		
	               	
	                <p>It has been said that flowers have meaning.
					A famous story teller, <b>Hans Christian Anderson</b> once said; <i>"Just Living is not enough.. one must have sunshine, freedom and a little flower."</i>
					Flowers may be small and evanescent, but people have loved them since the ancient time.<br><br>

					Flower Studio is home of the skilled and talented floral artists, committed to enhance the magical charm of flower. 
					Creating the most exellent contemporary floral arrangement to meet the expectations of the customers is our mission.<br><br>

					Flower represents your feeling more than anything. Whenever you need to express something to your beloved, we are pleased to provide solutions for you.
					Both personal and corporate need are welcomed.</p>
            	</article>				
            </div>
		</section>

<?php include("includes/footer.php"); ?>

