<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Flower Studio</title>
	<meta name="author" content="Flower Studio" />

	<!-- <meta name="description" content="Qbig Games merupakan mobile game platform yang memberikan pilihan game-game mobile yang seru dan penuh tantangan bagi gamers Indonesia.">
	<meta name="keywords" content="qiqigames, qiqi, game, games, qeon, qeon interactive, fps, casual, puzzle, gratis" />

	<meta property="og:title" content="Rex Regum Qeon Website by Qeon Interactive" />
	<meta property="og:site_name" content="Rex Regum Qeon Website by Qeon Interactive" />
	<meta property="og:description" content="Qbig Games merupakan mobile game platform yang memberikan pilihan game–game mobile yang seru dan penuh tantangan bagi gamers Indonesia." />
	<meta property="og:image" content="http://qbiggames.qeon.co.id/assets/images/logo--rrq.png" /> -->

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="assets/images/favicon.ico">
	<!-- Custom css -->
	<link type="text/css" rel="stylesheet" href="assets/css/styles.css"/>
</head>

<body class="main__error__body">
	<div class="main__error__wrapper">
		<div class="main__error">
			<div class="main__error--logo logo--flowerstudio"></div>
			<hr class="main__error--hr">
			<ul class="main__error--ul">
				<li class="main__error--li">
					<img src="assets/images/img--error.png" alt="" class="main__error--imgflower">
				</li>
				<li class="main__error--li">
					<h1 class="main__error--h1">404</h1>
					<p class="main__error--p">Sorry ! That page doesn’t seem to exist !</p>
					<br>
					<a class="button--backtohome" href="index.php">BACK TO HOME</a>
				</li>
			</ul>
		</div>
	</div>
</body>


