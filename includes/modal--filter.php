<!-- ajax service search -->
<script>
    //<![CDATA[
    var services = [
    {
        "id": 1,
        "link": "2-darien-hoeger",
        "name": "Darien Hoeger",
        "price": 201,
        "image": "assets/images/products/img--flower1.png",
        "category": "boquet",
        "color": "white",
        "ocassion": "birthday"

    },
    {
        "id": 2,
        "link": "3-mrs-frederique-kris",
        "name": "Mrs. Frederique Kris",
        "price": 500,
        "image": "assets/images/products/img--flower1.png",
        "category": "table-arrangement",
        "color": "mix",
        "ocassion": "wedding"
    },
    {
        "id": 3,
        "link": "4-jedediah-pouros",
        "name": "Jedediah Pouros",
        "price": 300,
        "image": "assets/images/products/img--flower1.png",
        "category": "wedding",
        "color": "pink",
        "ocassion": "anniversary"
    },
    {
        "id": 4,
        "link": "5-mrs-daisy-macejkovic",
        "name": "Mrs. Daisy Macejkovic",
        "price": 410,
        "image": "assets/images/products/img--flower1.png",
        "category": "boquet",
        "color": "peach",
        "ocassion": "babygift"
    } ];
    //]]>
</script>

<script id="modal__product--li" type="text/html">
    <li class="product--li">
    	<a href="<%= link %>" class="product--a">
            <img class="product--img" src="assets/images/bx_loader.gif" alt="" data-echo="<%= image %>" alt="">
            <h3 class="product--title"><%= name %></h3>
            <h4 class="product--price">Rp <%= price %></h4>
        </a>
  	</li>
</script>