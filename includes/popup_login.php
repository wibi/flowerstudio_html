
	<!-- popup login -->
	<section class="popup__login--wrapper popup--wrapper popup--hide" id="popup__login">
		<div class="popup__login--overlay popup--overlay"></div>
	    <div class="popup__login">
	        <h2 class="popup__login--h2">LOGIN</h2>
	        <span class="info__alert alert--failed"><span class="icon--alert--failed"></span>Invalid email / password. Please double-check your LOGIN information and try again.</span>
	        <form class="popup__login--form" action="">
	        	<label for="" class="product__sorting--label">Email</label>
				<input class="popup__login--input" type="text"><br>
	        	<label for="" class="product__sorting--label">Password</label>
	        	<input class="popup__login--input" type="text"><br><br>
	        	<a href="" class="button button--login--popup">LOGIN</a><font class="popup__login--font">or <a href="">Sign Up</a></font>
	        	<span class="popup__login--close popup--close"></span><br>
	        	<a href="" class="popup__login--forgot">Forgot my password</a>
	        </form>
	    </div>
	</section>