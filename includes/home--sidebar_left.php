
<!-- home sidebar left -->
<section class="home__sidebar__left">
	<div class="sidebar__left__box choose__by__category">
		<span class="sidebar__left__box--title collapse__link--cat"><h4>Choose by Category</h4><span class="icon--collapse--cat icon--collapse--min"></span></span>
        <form class="sidebar__left__box--form collapse__container--cat" action="">
            <ul class="sidebar__left__box--ul">
				<li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Table Arrangement</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category2.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category3.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category4.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category5.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category6.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category7.png" alt="">Boquet</a>
                </li>
            </ul>
        </form>
	</div>
	<div class="sidebar__left__box choose__by__colour">
		<span class="sidebar__left__box--title collapse__link--col"><h4>Choose by Colour</h4><span class="icon--collapse--col icon--collapse--min"></span></span>
		<form class="sidebar__left__box--form collapse__container--col" action="">
          	<ul class="sidebar__left__box--ul">
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
          	</ul>
        </form>
	</div>
	<div class="sidebar__left__box choose__by__ocassions">
		<span class="sidebar__left__box--title collapse__link--oca"><h4>Choose by Ocassions</h4><span class="icon--collapse--oca icon--collapse--min"></span></span>
		<form class="sidebar__left__box--form collapse__container--oca" action="">
          	<ul class="sidebar__left__box--ul">
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
                <li class="sidebar__left__box--li">
                    <a class="sidebar__left__box--a" href=""><img src="assets/images/sidebarleft__home/icon--sidebarleft__home--category1.png" alt="">Boquet</a>
                </li>
          	</ul>
        </form>
	</div>
</section>