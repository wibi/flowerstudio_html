        
        <!-- button footer mobile -->
		<center><a class="button--footer--responsive collapse__link--footer">FOOTER <span class="icon--collapse--footer icon--collapse--min"></span></a></center>

        <!-- footer -->
		<section class="footer collapse__container--footer "> <!-- bg--blur & hide--footer --> 
			<div class="footer__menu__wrapper">
				<nav class="footer__menu--nav">
					<ul class="footer__menu--ul">
						<li class="footer__menu--li">
							<h3 class="footer__menu--h3">CONNECT</h3>
							<ul class="link__sosmed--ul">
								<li class="link__sosmed--li"><a class="link__sosmed--a" href=""><img class="link__sosmed--img" src="assets/images/logo--fb--mini.png" alt=""></a></li>
								<li class="link__sosmed--li"><a class="link__sosmed--a" href=""><img class="link__sosmed--img" src="assets/images/logo--twit--mini.png" alt=""></a></li>
								<li class="link__sosmed--li"><a class="link__sosmed--a" href=""><img class="link__sosmed--img" src="assets/images/logo--instagram--mini.png" alt=""></a></li>
							</ul>							
						</li>
						<li class="footer__menu--li">
							<h3 class="footer__menu--h3">HELP&amp;MORE</h3>
							<ul class="footer__menu__link--ul">
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="">Home</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="">About Us</a></li>
							</ul>							
						</li>
						<li class="footer__menu--li">
							<h3 class="footer__menu--h3">PRODUCTS</h3>
							<ul class="footer__menu__link--ul">
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Bouquet</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Label Arrangement</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Standing Flower</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Bunga Papan</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Decoration</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Wedding</a></li>
								<li class="footer__menu__link--li"><a class="footer__menu__link--a" href="#">Pot Plant</a></li>
							</ul>							
						</li>
						<li class="footer__menu--li">
							<center><img class="" src="assets/images/logo--flowerstudio.png" alt="" width="54px" hegiht="" style="margin-bottom:5px;"></center>
							<center><img class="" src="assets/images/logo--veritrans.png" alt="" width="116px"></center>
							<center>
								<small class="footer__menu--li--small">Payment Security :</small><br>
								<ul class="footer__menu__link--ul security--ul">
									<li class="footer__menu__transaction--li"><img class="logo__transaction" src="assets/images/logo--visa--mini.png" alt=""></li>
									<li class="footer__menu__transaction--li"><img class="logo__transaction" src="assets/images/logo--mastercard--mini.png" alt=""></li>
								</ul>
							</center>
							<center>
								<small class="footer__menu--li--small">Payment Methods :</small><br>
								<ul class="footer__menu__link--ul">								
									<li class="footer__menu__transaction--li"><img class="logo__transaction" src="assets/images/logo--cimb--mini.png" alt=""></li>
									<li class="footer__menu__transaction--li"><img class="logo__transaction" src="assets/images/logo--bca--mini.png" alt=""></li>
									<li class="footer__menu__transaction--li"><img class="logo__transaction" src="assets/images/logo--mandiri--mini.png" alt=""></li>
								</ul>	
							</center>						
						</li>
						<li class="footer__menu--li">
							<h3 class="footer__menu--h3">JOIN OUR NEWSLETTER!</h3>
							<form action="">
                              	<!-- first name -->
                              	<div class="form__content--divwrapper">
                                    <span class="form__content--subscribe">
                                      	<input class="input--subscribe" type="text" placeholder="enter your email address here">
                                      	<a href="" class="button--subscribe link--subscribe">SUBSCRIBE</a>	 
                                    </span><br>
                                    <span class="info__alert alert--success"><span class="icon--alert--success"></span>Thank you! your email has been added</span>
                                    <span class="info__alert alert--failed"><span class="icon--alert--failed"></span>Please enter email address</span>
                              	</div>	
                            </form>					
						</li>
					</ul>
				</nav><div style="clear:both;"></div>
				<small class="footer__menu--copyright">Flower Studio Your Partner in Sophisticated Life, &copy; 2014, All Right Reserved</small>
			</div>

		</section>	

		<!-- header -->
		<?php include("popup_login.php"); ?>

		<!-- popup search -->
	  	<?php include("includes/popup_search.php"); ?>

	</div>
	<!-- /container -->

</body>

	<!-- JS -->
	<!-- Latest compiled and minified JavaScript -->
	<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>-->
	<script type="text/javascript" src="assets/js/jquery-1.11.1.js"></script>	
	<!--<script type="text/javascript" src="assets/js/jquery-1.3.2.js" ></script> 
	<script>var jq132 = $.noConflict(true);</script> -->	
	<!-- <script type="text/javascript" src="assets/js/min/corejs.min.js"></script> -->
	<!-- In corejs -->
	<script type="text/javascript" src="assets/js/component/bxslider.js"></script>
	<script type="text/javascript" src="assets/js/component/easing.1.3.js"></script>
	<script type="text/javascript" src="assets/js/component/fitvids.js"></script>
	<script type="text/javascript" src="assets/js/component/ready-20140222.min.js"></script>
	<script type="text/javascript" src="assets/js/component/W-1.0.1.min.js"></script>
	<script type="text/javascript" src="assets/js/component/molt.js"></script>
	<script type="text/javascript" src="assets/js/component/echo.min.js"></script>		

	<script type="text/javascript" src="assets/js/component/scripts.js"></script>

	<!-- Google Analytic -->
	<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-56888154-2', 'auto');
		 ga('send', 'pageview');
	</script>

</html>

