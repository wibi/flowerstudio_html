
<!-- sidebar left -->
<section class="products__sidebar__left">
	<div class="sidebar__left__box choose__by__category">
		<span class="sidebar__left__box--title collapse__link--cat"><h4>Choose by Category</h4><span class="icon--collapse--cat icon--collapse--min"></span></span>
        <form class="sidebar__left__box--form collapse__container--cat" action="">
            <ul class="sidebar__left__box--ul" id="category">
				<li class="sidebar__left__box--li"><input id="cat--1" type="checkbox" name="checkbox__category" value="all" checked><label for="cat--1">All</label></li>
				<li class="sidebar__left__box--li"><input id="cat--2" type="checkbox" name="checkbox__category" value="boquet"><label for="cat--2">Boquet</label></li>
				<li class="sidebar__left__box--li"><input id="cat--3" type="checkbox" name="checkbox__category" value="table-arrangement"><label for="cat--3">Table Arrangement</label></li>
				<li class="sidebar__left__box--li"><input id="cat--4" type="checkbox" name="checkbox__category" value="standing-flower"><label for="cat--4">Standing Flower</label></li>
				<li class="sidebar__left__box--li"><input id="cat--5" type="checkbox" name="checkbox__category" value="bunga-papan"><label for="cat--5">Bunga papan</label></li>
				<li class="sidebar__left__box--li"><input id="cat--6" type="checkbox" name="checkbox__category" value="decoration"><label for="cat--6">Decoration</label></li>
				<li class="sidebar__left__box--li"><input id="cat--7" type="checkbox" name="checkbox__category" value="wedding"><label for="cat--7">Wedding</label></li>
				<li class="sidebar__left__box--li"><input id="cat--8" type="checkbox" name="checkbox__category" value="pot-plant"><label for="cat--8">Pot plant</label></li>
            </ul>
        </form>
	</div>
	<div class="sidebar__left__box choose__by__colour">
		<span class="sidebar__left__box--title collapse__link--col"><h4>Choose by Colour</h4><span class="icon--collapse--col icon--collapse--min"></span></span>
		<form class="sidebar__left__box--form collapse__container--col" action="">
          	<ul class="sidebar__left__box--ul" id="color">
                <li class="sidebar__left__box--li"><input id="col--1" type="checkbox" name="checkbox__colour" value="all" checked><label for="col--1">All</label></li>
                <li class="sidebar__left__box--li"><input id="col--2" type="checkbox" name="checkbox__colour" value="white"><label for="col--2">White Flowers</label></li>
                <li class="sidebar__left__box--li"><input id="col--3" type="checkbox" name="checkbox__colour" value="peach"><label for="col--3">Peach Flowers</label></li>
                <li class="sidebar__left__box--li"><input id="col--4" type="checkbox" name="checkbox__colour" value="orange"><label for="col--4">Orange Flowers</label></li>
                <li class="sidebar__left__box--li"><input id="col--5" type="checkbox" name="checkbox__colour" value="pink"><label for="col--5">Pink Flowers</label></li>
                <li class="sidebar__left__box--li"><input id="col--6" type="checkbox" name="checkbox__colour" value="red"><label for="col--6">Red Flowers</label></li>
                <li class="sidebar__left__box--li"><input id="col--7" type="checkbox" name="checkbox__colour" value="mix"><label for="col--7">Mix Flowers</label></li>
          	</ul>
        </form>
	</div>
	<div class="sidebar__left__box choose__by__ocassions">
		<span class="sidebar__left__box--title collapse__link--oca"><h4>Choose by Ocassions</h4><span class="icon--collapse--oca icon--collapse--min"></span></span>
		<form class="sidebar__left__box--form collapse__container--oca" action="">
          	<ul class="sidebar__left__box--ul" id="ocassion">
                <li class="sidebar__left__box--li"><input id="oca--1" type="checkbox" name="checkbox__ocassions" value="all" checked><label for="oca--1">All</label></li>
                <li class="sidebar__left__box--li"><input id="oca--2" type="checkbox" name="checkbox__ocassions" value="birthday"><label for="oca--2">Birthday</label></li>
                <li class="sidebar__left__box--li"><input id="oca--3" type="checkbox" name="checkbox__ocassions" value="wedding"><label for="oca--3">Wedding</label></li>
                <li class="sidebar__left__box--li"><input id="oca--4" type="checkbox" name="checkbox__ocassions" value="anniversary"><label for="oca--4">Anniversary</label></li>
                <li class="sidebar__left__box--li"><input id="oca--5" type="checkbox" name="checkbox__ocassions" value="celebration"><label for="oca--5">Celebration</label></li>
                <li class="sidebar__left__box--li"><input id="oca--6" type="checkbox" name="checkbox__ocassions" value="farewellfor"><label for="oca--6">Farewell for</label></li>
                <li class="sidebar__left__box--li"><input id="oca--7" type="checkbox" name="checkbox__ocassions" value="simpathy"><label for="oca--7">Sympathy</label></li>
                <li class="sidebar__left__box--li"><input id="oca--8" type="checkbox" name="checkbox__ocassions" value="babygift"><label for="oca--8">Baby's gift</label></li>
                <li class="sidebar__left__box--li"><input id="oca--9" type="checkbox" name="checkbox__ocassions" value="bussiness"><label for="oca--9">Bussiness</label></li>
          	</ul>
        </form>
	</div>
	<div class="sidebar__left__box choose__by__price">
		<span class="sidebar__left__box--title collapse__link--price"><h4>Choose by Price</h4><span class="icon--collapse--price icon--collapse--min"></span></span>
		<form class="sidebar__left__box--form collapse__container--price" action="">
          	<ul class="sidebar__left__box--ul" id="price">
                <li class="sidebar__left__box--li"><input id="price--1" type="radio" name="radio__price" value="" checked><label class="custom--label" for="price--1">All</label></li>
                <li class="sidebar__left__box--li"><input id="price--2" type="radio" name="radio__price" value=""><label class="custom--label" for="price--2">under Rp 250.000</label></li>
                <li class="sidebar__left__box--li"><input id="price--3" type="radio" name="radio__price" value=""><label class="custom--label" for="price--3">Rp 250.000 - Rp 500.000</label></li>
                <li class="sidebar__left__box--li"><input id="price--4" type="radio" name="radio__price" value=""><label class="custom--label" for="price--4">Rp 500.000 - Rp 750.000</label></li>
                <li class="sidebar__left__box--li"><input id="price--5" type="radio" name="radio__price" value=""><label class="custom--label" for="price--5">Rp 750.000 - Rp 1.000.000</label></li>
                <li class="sidebar__left__box--li"><input id="price--6" type="radio" name="radio__price" value=""><label class="custom--label" for="price--6">Above Rp 1.000.000</label></li>
          	</ul>
        </form>
	</div>
</section>