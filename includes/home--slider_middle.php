
<!-- slider center wrapper -->
<section class="home__slider__middle">

    <!-- slider recommended -->
    <section class="slider__recommended__wrapper">
        <span class="slider__recommended--title"><h3>RECOMMENDED</h3></span>
        <div class="slider__recommended slider__middle">
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                </a>
            </div>
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                </a>
            </div>
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller3.png">
                </a>
            </div>
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                </a>
            </div>
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                </a>
            </div>
            <div>
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                </a>
            </div>
        </div>
    </section>

    <!-- slider corporate -->
    <section class="slider__corporate__wrapper">
        <span class="slider__corporate--title"><h3>CORPORATE</h3></span>
        <div class="slider__corporate slider__middle">
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller3.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
        </div>
    </section>

    <!-- slider special gifts -->
    <section class="slider slider__specialgifts__wrapper">
        <span class="slider__corporate--title"><h3>SPECIAL GIFTS</h3></span>
        <div class="slider__specialgifts slider__middle">
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller3.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
        </div>
    </section>

    <!-- slider wedding world -->
    <section class="slider slider__weddingworld__wrapper">
        <span class="slider__corporate--title"><h3>WEDDING WORLD</h3></span>
        <div class="slider__weddingworld slider__middle">
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller3.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller4.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller1.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
            <div class="">
                <a href="" class="">
                    <img class="lazy" src="assets/images/bx_loader.gif" data-src="assets/images/slider__thebest__seller/img--slider__thebest__seller2.png">
                    <h5 class="slider__corporate--name">WEEKLY FLOWER</h5>
                    <h5 class="slider__corporate--type">Rp 123.456,-</h5>
                </a>
            </div>
        </div>
    </section>

</section>