
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper">
			<center>
				<img class="molt banner--article" 
				data-molt-0w="assets/images/img__banner__page/banner--flowercourse--320.jpg"
		  		data-molt-480w="assets/images/img__banner__page/banner--flowercourse--768.jpg"
		  		data-molt-768w="assets/images/img__banner__page/banner--flowercourse--1360.jpg" 
		  		data-molt-1400w="assets/images/img__banner__page/banner--flowercourse--1920.jpg" alt="">
			</center>
            <div class="main__container" style="display:block !important;">
            	<p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> About Us</a></p>
            	<h1>Flower Arrangement Courses</h1>
            	<article class="main__container--article">
	                <p>Let’s have fun and explore your creativity with us!!<br><br>

					In our flower arrangement class, we provide the cut flowers, vases, floral cutters and step by step instructions on how to recognize the freshest flowers, 
					how to care for them and how to create a beautiful flower arrangement.<br><br>

					Each of you will then be hands on with your own set of flowers to create a 
					gorgeous bouquet to keep or to give to someone special.
					Lessons usually last about 1-2 hours.<br>

					<h2>Private and Custom Group Courses</h2>
					Private lessons or custom classes for groups are available. If you have a free time, feel free  to call us for a private class. 
					Or if you want to have a group course with your group, family, colleagues or friends, we can arrange an interesting class for you. 
					<b>Start from only Rp.100.000,-/person</b><br><br>
					
					<h2>Flower Course at Your Venue</h2>
					<ol>
						<li>
							<b>Lunch or afternoon tea package</b><br>
						    It’s a very interesting class where participants have fun, meet new friends and  
						    find their inner creativity.  The packages already include lunch or afternoon tea.
						    Lunch package:<br>
						    Place	: Bistronomy Restaurant<br>
						    Time	: 10.30<br>
						    Fee	: Rp. 400.000,-<br><br>
						</li>
						<li>
							<b>Afternoon tea package</b><br>
						    Place	: Bistronomy Restaurant<br>
						    Time	: 14.00<br>
						    Fee	: Rp.350.000,-<br>
						</li>
					</ol>

					Please call us to customize your lesson :<br>
					Flower Studio  <i>+62 21 570 3666</i> or email at <i><a href="mailto:info@flowerstudio.co.id">info@floristflowerstudio.co.id</a></i>

					</p>
            	</article>				
            </div>
		</section>

<?php include("includes/footer.php"); ?>

