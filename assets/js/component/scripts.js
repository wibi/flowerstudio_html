
$(document).ready(function(){

    /////// bxslider -- home banner
    $('.slider__banner').bxSlider({        
        captions: true,
        mode: 'fade'
    });

    // bxslider -- bestseller
    var page = 0;
    var maxSlides = 5;
    var slider = $('.slider__bestseller').bxSlider({
        minSlides: 1,
        maxSlides: maxSlides,
        touchEnabled: true,
        startSlide: page,
        onSliderLoad: function () {
            var $sliderImgs = $(".slider__bestseller img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        },
        onSlideBefore: function () {
            page = slider.getCurrentSlide();
            var $sliderImgs = $(".slider__bestseller img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        }
    });
    
    // bxslider -- recommended
    var slider = $('.slider__recommended').bxSlider({
        minSlides: 1,
        maxSlides: maxSlides,
        controls: false,
        touchEnabled: true,
        startSlide: page,
        onSliderLoad: function () {
            var $sliderImgs = $(".slider__recommended img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        },
        onSlideBefore: function () {
            page = slider.getCurrentSlide();
            var $sliderImgs = $(".slider__recommended img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        }
    });

    // bxslider -- corporate
    var slider = $('.slider__corporate').bxSlider({
        minSlides: 1,
        maxSlides: maxSlides,
        touchEnabled: true,
        startSlide: page,
        onSliderLoad: function () {
            var $sliderImgs = $(".slider__middle img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        },
        onSlideBefore: function () {
            page = slider.getCurrentSlide();
            var $sliderImgs = $(".slider__middle img");
            var start = page * maxSlides + maxSlides;
            var stop = start + maxSlides;
            for (var i = start; i < stop; i++) {
                var selecter = '[data-src="' + $sliderImgs.eq(i).data('src') + '"]';
                $(selecter).attr('src', $(selecter).data('src')).one('load', function () {
                    $(this).removeClass('lazy');
                });
            }
        }
    });
    
    // bxslider -- specialgifts
    var slider = $('.slider__specialgifts').bxSlider({
        minSlides: 1,
        maxSlides: maxSlides,
        touchEnabled: true,
        startSlide: page
    });
    
    // bxslider -- weddingworld
    var slider = $('.slider__weddingworld').bxSlider({
        minSlides: 1,
        maxSlides: maxSlides,
        touchEnabled: true,
        startSlide: page
    });

    /////// lazy load echo
    echo.init({
        offset: 100,
        throttle: 250,
        // unload: false,
        // callback: function (element, op) {
        //   console.log(element, 'has been', op + 'ed')
        // }
    });
    
    /////// popup login
    $(".menu__account--ul li .button--login").click(function(event) {
        event.preventDefault();
        var popuplogin = $(this).attr("href");
        $(popuplogin).addClass("popup--show");
        $(popuplogin).removeClass("popup--hide");
        $(".header").addClass("bg--blur");
        $(".main__container__wrapper").addClass("bg--blur");
        $(".main__container__wrapper").show();
        $(".popup__search--wrapper").css("position", "absolute");
        if ($(window).width() > 700) {            
            $(".footer").addClass("bg--blur");
            $(".footer").show();
        } 
        if ($('.popup--wrapper').hasClass('popup--show')) {
            $(".popup__search--wrapper").removeClass("popup--show");
            $(".popup__search--wrapper").addClass("popup--hide");
        }        
    });
    /////// popup search
    $(".menu__account--ul li .popup--search--a").click(function(event) {
        event.preventDefault();
        echo.init({
            offset: 100,
            throttle: 250,
            // unload: false,
            // callback: function (element, op) {
            //   console.log(element, 'has been', op + 'ed')
            // }
        });
        var popupsearch = $(this).attr("href");
        $(popupsearch).addClass("popup--show");
        $(popupsearch).removeClass("popup--hide");
        $(".popup__search--wrapper").css("position", "relative");
        $(".main__container__wrapper").hide(); 
            if ($(window).width() < 700) {
                $(".collapse__link--footer").hide();
            }
            if ($(window).width() > 700) {
                $(".footer").hide();
            }                    
    });    
    /////// close popup search & login
    $(".popup--close").click(function(event) {
        $(".popup--wrapper").addClass("popup--hide");
        $(".popup--wrapper").removeClass("popup--show");
        $(".header").removeClass("bg--blur");
        $(".main__container__wrapper").removeClass("bg--blur");        
        $(".main__container__wrapper").show();
        $(".popup__search--wrapper").css("position", "absolute");        
            if ($(window).width() < 700) {
                $(".collapse__link--footer").show();
            }
            if ($(window).width() > 700) {
                $(".footer").removeClass("bg--blur");
                $(".footer").show();
            } 
    });
    $(".popup--overlay").click(function(event) {
        $(".popup--wrapper").addClass("popup--hide");
        $(".popup--wrapper").removeClass("popup--show");
        $(".header").removeClass("bg--blur");
        $(".main__container__wrapper").removeClass("bg--blur");        
        if ($(window).width() > 700) {
            $(".footer").removeClass("bg--blur");
        } 
    });

    /////// menu header responsive
    $('.menu__responsive').hide();
    $('.logo--menu').click(function(){
        $('.menu__responsive').slideToggle('slow');
    }); 

    if ($(window).width() < 700) {
        /////// account name in header
        $(".accountname--link").click(function(event) {
            $(".menu--accountname__responsive--ul").slideToggle("slow");
        });
    }
    if ($(window).width() > 700) {
        /////// account name in header
        $(".accountname--link").click(function(event) {
            if ($(".menu--accountname--ul").hasClass("hide--menu--accountname")){
                $(".menu--accountname--ul").removeClass("hide--menu--accountname");
                $(".menu--accountname--ul").addClass("show--menu--accountname");
            }
            else{
                $(".menu--accountname--ul").removeClass("show--menu--accountname");
                $(".menu--accountname--ul").addClass("hide--menu--accountname");
            }
            
        });
    }

    if ($(window).width() < 1024) {
        /////// when click, collapse category
        $(".collapse__link--cat").click(function(event) {
            $(".collapse__container--cat").slideToggle("slow");
            if ($(".icon--collapse--cat").hasClass("icon--collapse--min")){
                $(".icon--collapse--cat").removeClass("icon--collapse--min");
                $(".icon--collapse--cat").addClass("icon--collapse--max");
            }
            else{
                $(".icon--collapse--cat").removeClass("icon--collapse--max");
                $(".icon--collapse--cat").addClass("icon--collapse--min");
            }
        });
        /////// when click, collapse color
        $(".collapse__link--col").click(function(event) {
            $(".collapse__container--col").slideToggle("slow");
            if ($(".icon--collapse--col").hasClass("icon--collapse--min")){
                $(".icon--collapse--col").removeClass("icon--collapse--min");
                $(".icon--collapse--col").addClass("icon--collapse--max");
            }
            else{
                $(".icon--collapse--col").removeClass("icon--collapse--max");
                $(".icon--collapse--col").addClass("icon--collapse--min");
            }
        });
        /////// when click, collapse ocassion
        $(".collapse__link--oca").click(function(event) {
            $(".collapse__container--oca").slideToggle("slow");
            if ($(".icon--collapse--oca").hasClass("icon--collapse--min")){
                $(".icon--collapse--oca").removeClass("icon--collapse--min");
                $(".icon--collapse--oca").addClass("icon--collapse--max");
            }
            else{
                $(".icon--collapse--oca").removeClass("icon--collapse--max");
                $(".icon--collapse--oca").addClass("icon--collapse--min");
            }
        });
        /////// when click, collapse price
        $(".collapse__link--price").click(function(event) {
            $(".collapse__container--price").slideToggle("slow");
            if ($(".icon--collapse--price").hasClass("icon--collapse--min")){
                $(".icon--collapse--price").removeClass("icon--collapse--min");
                $(".icon--collapse--price").addClass("icon--collapse--max");
            }
            else{
                $(".icon--collapse--price").removeClass("icon--collapse--max");
                $(".icon--collapse--price").addClass("icon--collapse--min");
            }
        });
        
        /////// when click, collapse footer                
        $(".collapse__link--footer").click(function(event) {  
            $(".collapse__container--footer").slideToggle("slow");                                
                if ($(".icon--collapse--footer").hasClass("icon--collapse--min")){
                    $(".icon--collapse--footer").removeClass("icon--collapse--min");
                    $(".icon--collapse--footer").addClass("icon--collapse--max");
                    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
                    return false; 
                }
                else{
                    $(".icon--collapse--footer").removeClass("icon--collapse--max");
                    $(".icon--collapse--footer").addClass("icon--collapse--min");
                }
        });
    } else {
        // 

    }         

    /////// responsive img
    molt($('.molt')).start();

    /////// tab payment
    $(".payment__method__tab--ul li:first").addClass("payment__method__tab--active");
    $(".payment__method__tab--ul li a").click(function(event) {
        event.preventDefault();
        $(this).closest('li').addClass("payment__method__tab--active").siblings().removeClass("payment__method__tab--active");
        var tab = $(this).attr("href");
        $(".payment__method__content").not(tab).css("display", "none");
        $(tab).fadeIn();
        if($(this).attr("id") == "internetbanking--tab") {
            $(".button--cc--transfer").hide();            
            $(".button--next--cimb").fadeIn();
        }
        else if($(this).attr("id") == "creditcard--tab") {
            $(".button--next--cimb").hide();
            $(".button--cc--transfer").fadeIn();
            $(".button--cc--transfer").css("display", "inline-block");
        }
        else if($(this).attr("id") == "transfer--tab") {
            $(".button--next--cimb").hide();
            $(".button--cc--transfer").fadeIn();
            $(".button--cc--transfer").css("display", "inline-block");
            $(".adjustment__code__payment").fadeIn();            
        }
        else{
            $(".adjustment__code__payment").hide();
        }
    });
    
    /////// tab account
    $(".account__right--ul li:first").addClass("account__right--active");
    $(".account__right--ul li a").click(function(event) {
        event.preventDefault();
        $(this).closest('li').addClass("account__right--active").siblings().removeClass("account__right--active");
        var tab = $(this).attr("href");
        $(".tab--account").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    
    /////// when click, show invoice
    $(".table__orderhistory--list td a").click(function(event) {
        event.preventDefault();
        var tab = $(this).attr("href");
        $(".invoice__data__wrapper").not(tab).css({"display":"none"});
        $(tab).fadeIn('slow');
    });
    

    /* Dropdownlist event */

    var data_dropdown = new Array();
    $('#province').change(function(){
        change_web();
        
      });

    $('#city').change(function(){
        change_area();
    });

    $('#area').change(function(){
        change_cost();
    });


     function change_cost(selected)
    {
        // console.log("do change web");
        var select         = $("#area");
        var sel            = $("#shipping_cost");
      
        var costbottom     = $('#shipping_cost_input');
        var total          = $('#total');
        var grandtotal     = $('#grandtotal');
        var totalhidden    = $("#totalhidden");
        var labelcost      = $(".shipping__cost--price");
      
        var webid   = select.val();
        var url     = select.data("url");
       
            if(data_dropdown[webid] != undefined || data_dropdown[webid] != null)

            {
                sel.empty().append(data_dropdown[webid]);
                select.removeAttr("disabled");
                sel.removeAttr("disabled");
                if(selected !=null) sel.val(selected);
            }
            else 
            {
                $.get(url + "/" + webid,function(ret){

                    var data = ret;
                    if(data.result.status == true)
                    {
                       
                       // console.log(data.result.data);
                       // console.log(sel);
                       if(data.result.data != ""){
                          
                          // var totalprice = parseInt(data.result.data) +  parseInt(total.text);
                        
                          var grandtotals =  parseInt(total.text()) + parseInt(data.result.data);
                          console.log(grandtotals);  

                          // assign value to label text and  hidden text field
                          sel.text(data.result.data);

                          labelcost.text(data.result.data);
                          
                          sel.val(data.result.data);
                          grandtotal.text(grandtotals);

                          totalhidden.val(grandtotals);
                          costbottom.val(data.result.data);


                       }else{
                          sel.text('cannot find your area');
                          costbottom.text('cannot find your area');
                       }
                     
                    }else {
                         sel.text('error');
                         costbottom.text('error');
                    }
                });
            }
     }

    function change_area(selected)
    {
        console.log("do change web");
        var select = $("#city");
        var sel    = $("#area");
        select.attr("disabled","disabled");
        sel.attr("disabled","disabled");

        var webid   = select.val();
        var url     = select.data("url");
        console.log(url);

            if(data_dropdown[webid] != undefined || data_dropdown[webid] != null)

            {
                sel.empty().append(data_dropdown[webid]);
                select.removeAttr("disabled");
                sel.removeAttr("disabled");
                if(selected !=null) sel.val(selected);
            }
            else 
            {
                $.get(url + "/" + webid,function(ret){

                    var data = ret;
                    if(data.result.status == true)
                    {
                        var html = repopulate_data_dropdown(data.result.data);
                        data_dropdown[webid] = html;
                        sel.empty().append(data_dropdown[webid]);
                        select.removeAttr("disabled");
                        sel.removeAttr("disabled");
                        if(selected != null) sel.val(selected);
                    }else {
                        generate_alert(data.result.status,data.result.msg);
                    }
                });
            }
     }


    function change_web(selected)
    {
        console.log("do change web");
        var select = $("#province");
        var sel    = $("#city");
        select.attr("disabled","disabled");
        sel.attr("disabled","disabled");

        var webid   = select.val();
        var url     = select.data("url");

            if(data_dropdown[webid] != undefined || data_dropdown[webid] != null)

            {
                sel.empty().append(data_dropdown[webid]);
                select.removeAttr("disabled");
                sel.removeAttr("disabled");
                if(selected !=null) sel.val(selected);
            }
            else 
            {
                $.get(url + "/" + webid,function(ret){

                    var data = ret;
                    if(data.result.status == true)
                    {
                        var html = repopulate_data_dropdown(data.result.data);
                        data_dropdown[webid] = html;
                        sel.empty().append(data_dropdown[webid]);
                        select.removeAttr("disabled");
                        sel.removeAttr("disabled");
                        if(selected != null) sel.val(selected);
                    }else {
                        generate_alert(data.result.status,data.result.msg);
                    }
                });
            }
     }


    function repopulate_data_dropdown(data)
    {
        var html = "";
        $.each(data, function(){
            html += "<option value='" + this.id + "'>" + this.name + "</option>";
        });
        return html;
    }


    function generate_alert(status, msg)
    {
        var html = "";
        html += "<div class='pure-form__line'>"
            html += "<label></label>";
            if(status != true)
            {
                html += "<div class='notification fail canhide career-notif'>" + msg + "</div>";
            } else {
                html += "<div class='notification success canhide career-notif'>" + msg + "</div>";
            }
        html += "</div>";
        return html;
    }

    /////// load maps
    function initialize() {
        var mapCanvas = document.getElementById('map_canvas');
        var mapOptions = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
    }
    google.maps.event.addDomListener(window, 'load', initialize);

});