
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper">
			<center>
				<img class="molt banner--article" 
				data-molt-0w="assets/images/img__banner__page/banner--terms--320.jpg"
		  		data-molt-480w="assets/images/img__banner__page/banner--terms--768.jpg"
		  		data-molt-768w="assets/images/img__banner__page/banner--terms--1360.jpg" 
		  		data-molt-1400w="assets/images/img__banner__page/banner--terms--1920.jpg" alt="">
			</center>
            <div class="main__container" style="display:block !important;">
            	<p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> About Us</a></p>
            	<h1>Terms and Condition</h1>
            	<article class="main__container--article">
	                <p>
						<h4>DETAIL INFORMATION</h4>
						To process your order, kindly fill all we need information such as your full name, email address, 
						contact number, your billing address, delivery address and payment details.<br><br>

						<b>TERM OF PAYMENT</b><br>
						Payment can be made via transfer or via cash.<br><br>

						Payment via Bank transfer :<br>
						<b>MANDIRI BANK</b><br>									
						Beneficiary Name	: PT. MID DUTA INTERNATIONAL<br>
						Bank Name		: MANDIRI BANK<br>
						Branch			: Midplaza Jakarta<br>
						A/C				: 122.00.0491443.1<br><br>

						<b>BCA BANK</b><br>
						Beneficiary Name	: PT. MID DUTA INTERNATIONAL<br>
						Bank Name		: Bank Central Asia<br>
						Branch			: Wisma 46<br>
						A/C				: 5255.3482.11<br><br>

						Online Payment can be made via Veritrans.<br>
						Veritrans<br><br> 

						Orders will not be dispatched until payment in full has been received.<br>

						<h4>DELIVERY PROCESS</h4>

						<b>PROCESS</b><br>
						To avoid delays with processing your order, please ensure your credit card number is accurately entered into our secure online ordering system. We will endeavour to contact you as soon as possible if there are any issues, however please be advised that your order cannot be dispatched until payment has been confirmed.<br>
						It is your obligation to enter the correct delivery address details at the time of ordering. If the delivery details you have given are incorrect Flower Studio will not refund. Should you wish to send the flowers again it will be at full expense to the customer.<br>
						Normal delivery hours for Flower Studio are between 9am and 5pm Mon-Sat. Flower Studio cannot guarantee a specific delivery time. We will make every attempt to fulfill your request but cannot guarantee it. As Flower Studio is closed on public holidays, we will deliver your gift on the next business day.<br><br> 

						<b>CHARGE</b><br>
						Please choose the delivery charges that you wished the flower arrangements to be delivered in the table SHOPPING BASKET above.<br>
						If you need further assistance please contact our Customer Care Team on <b>(+61 21) 570 3666)</b> or email: <a href="mailto:info@flowerstudio.co.id" target="_blank"><i>info@flowerstudio.co.id</i></a><br>
						Flower arrangements to be delivered in the table SHOPPING BASKET above. Your total amount of purchase depend on the delivery area that you pick out after we receive your order notification.<br>

						<b>DELIVERY CHARGE</b><br>
						Only one delivery is allowed for every purchase. Please choose the delivery charges that you wished the flower arrangements to be delivered in the table SHOPPING BASKET above. Your total amount of purchase depend on the delivery area that you pick out after we receive your order notification.<br>
						If you need further assistance please contact our Customer Care Team on <b>(+61 21) 570 3666)</b> or email: <a href="mailto:info@flowerstudio.co.id" target="_blank"><i>info@flowerstudio.co.id</i></a><br>
						Please be sure to read our delivery policies as follows:<br>
						* We deliver 7 Days a week 365 days a year<br>
						* Our standard delivery hours are from 8.00am - 7.00pm<br>
						* Please state your preferred delivery time when you order however we are unable to guarantee<br>
						   AM or exact delivery times between our delivery hours of 8.00am - 7.00pm but we do guarantee<br> 
						   that the order will be delivered on the date you specify.<br>
						* We deliver to all residenial, commercial, retail, government and public addresses.<br>
						* When you order please include your recipients (receivers) contact telephone number in case they * are not available when we attempt delivery.<br>
						<b>Please Note - We will ONLY call if there is a delivery problem.</b><br><br>

						<b>CANCELLATION</b><br>
						Cancellation of the order must be made at least 3 days prior to the delivery date. We will retain 50% of your deposit if the cancellation is not made on time.<br>
						Please note that by accessing, using or browsing this web-site you agree to be bound by its terms, conditions, disclaimers and limitations of liability ("Terms and Conditions"). Flower Studio reserves the right to amend or update such terms, conditions, disclaimers and limitations of liability at any time without providing notice to you. By using the web-site, you acknowledge that you have read and understood these Terms and Conditions.<br><br>

						If you purchase goods and services from bungabalirose.com or create a customer registration account with us, we may require you to provide your name, address, email address, credit card number or other details ("customer information"). We may subsequently require further details to enable the processing of any orders that you make. You agree to provide Flower Studio with current, complete and accurate customer information when asked to do so by the web-site or an employee of Flower Studio.<br><br>

						<b>REFUND/RETURN</b><br>
						We do not have to provide a refund if you have changed your mind about a particular purchase, please choose carefully.<br>
						If the goods are faulty, we will meet our obligations under the applicable laws. Flower Studio is committed to providing you products of high quality.<br><br>

						<b>WEDDING PURCHASE</b><br>
						We treat our Bride and Groom to be as our special clients in their special event. Therefore for Wedding Purchases, please make an inquiry of your color and style as you wish with our Wedding Collections. We will contact you after we receive your inquiry notification via e-mail or telephone.<br><br>

						<b>OTHER</b><br>
						Note that we may need to replace the flowers in the requested arrangement with other similar flowers with the same value should the flowers in the requested arrangement are not available.
					</p>
            	</article>				
            </div>
		</section>

<?php include("includes/footer.php"); ?>

