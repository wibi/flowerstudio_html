
<!-- header -->
<?php include("includes/header.php"); ?>

    <!-- main -->
    <section class="main__container__wrapper"> <!-- bg--blur -->
              <div class="main__container__2">
                                                
                    <!-- shipping wrapper -->
                    <section class="account__wrapper"> 
                              <div class="payment__step--main">
                                    
                                    <!-- success order payment -->
                                    <span class="payment__success">
                                        <h2 class="">CREATE A NEW PASSWORD</h2>
                                        <hr class="payment__success--hr">
                                        <p class="payment__success--p">
                                          Enter a new password for <a href="">abcd@email.com</a> below. Your password must be at least six characters long.
                                        </p><br>
                                        <div class="wrapper__form--resetpassword">
                                            <form action="">                                                      
                                                  <!-- New Password -->
                                                  <div class="form__content--divwrapper">
                                                        <label for="" class="form__content--label">New Password</label><font class="dot--dot"><b>:</b></font>
                                                        <input class="form__content--input" type="text"><br>
                                                  </div>                                                      
                                                  <!-- Confirm Password -->
                                                  <div class="form__content--divwrapper">
                                                        <label for="" class="form__content--label">Confirm Password</label><font class="dot--dot"><b>:</b></font>
                                                        <input class="form__content--input" type="text"><br>
                                                  </div>                                                      
                                                  <!-- SAVE -->
                                                  <br>
                                                  <div class="form__content--divwrapper">
                                                        <label for="" class="form__content--label hide--space--info"></label><font class="dot--dot"><b></b></font>
                                                        <a class="button--account2" href="">RESET PASSWORD</a><span class="form__content--info info--success">has been saved</span><br>
                                                  </div>                                                      
                                            </form>
                                        </div>
                                        
                                    </span>
                                     
                              </div>        
                    </section>
              </div>

              <!-- popup search -->
              <?php include("includes/popup_search.php"); ?>

    </section>

