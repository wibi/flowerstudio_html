
<!-- header -->
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper"> <!-- bg--blur -->
              <div class="main__container__2">
                  	<p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> Shopping Cart</a> / <a href="" class="breadscrumbs--a"> Shipping</a></p>
                                                
                    <!-- shipping wrapper -->
                  	<section class="account__wrapper"> 
                              <div class="payment__step--main">

                                    <!-- payment step -->
                                    <ul class="payment__step--ul">
                                          <li class="payment__step--li">
                                                <a class="payment__step--a">
                                                      <h4 class="payment__step--h4--after">Shopping Cart</h4>
                                                      <span class="icon--payment--after"></span>
                                                      <hr class="payment__step--hr--after">
                                                </a>
                                          </li>
                                          <li class="payment__step--li">
                                                <a class="payment__step--a">
                                                      <h4 class="payment__step--h4--now">Shipping</h4>
                                                      <span class="icon--payment--now"></span>
                                                      <hr class="payment__step--hr--after--half">
                                                </a>
                                          </li>
                                          <li class="payment__step--li">
                                                <a class="payment__step--a">
                                                      <h4 class="payment__step--h4--before">Payment</h4>
                                                      <span class="icon--payment--before"></span>
                                                </a>
                                          </li>
                                    </ul>
                                    
                                    <h2 class="payment__step--main--h2">PAYMENT METHODS</h2>
                                    <hr class="payment__step--main--hr">

                                    <div class="payment__step__right">
                                        <!-- gift note -->
                                        <div class="payment__step__right__giftnote__wrapper">
                                              <ul class="giftnote--ul">
                                                <li class="giftnote--li">This order is a gift, please omit billing details. Include a gift note below :</li>
                                              </ul>
                                              <p class="giftnote--info--desc">
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua.
                                              </p>
                                        </div>
                                        <!-- Orders information -->
                                        <div class="orderinformation__wrapper">
                                              <br>
                                              <b><p class="form__content--p">ORDER INFORMATION</p></b>
                                              <table class="table__orderinformation">
                                                    <!-- header -->
                                                    <tr class="table__orderinformation--header">
                                                          <th>Product</th>
                                                          <th>Qty</th>
                                                          <th>Price</th>
                                                    </tr>  
                                                    <!-- body -->
                                                    <tr class="table__orderinformation--list">
                                                          <td class="orderinformation--textleft">
                                                                Orange Rose Bouquet
                                                          </td>
                                                          <td>
                                                                80                                                              
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                Rp 250.000
                                                          </td>
                                                    </tr>
                                                    <tr class="table__orderinformation--list">
                                                          <td class="orderinformation--textleft">
                                                                Orange Rose Bouquet
                                                          </td>
                                                          <td>
                                                                80                                                              
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                Rp 250.000
                                                          </td>
                                                    </tr>
                                                    <tr class="table__orderinformation--total">
                                                          <td>
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                TOTAL                                                             
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                Rp 20.500.000
                                                          </td>
                                                    </tr>
                                                    <tr class="table__orderinformation--total">
                                                          <td class="orderinformation--textleft">
                                                                Shipping cost
                                                          </td>
                                                          <td>                                                                                                                     
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                Rp 500.000
                                                          </td>
                                                    </tr>
                                                    <!-- total -->
                                                    <tr class="table__orderinformation--total">
                                                          <td colspan="2" class="orderinformation--textright">
                                                                <b>GRAND TOTAL</b>
                                                          </td>
                                                          <td class="orderinformation--textright">
                                                                <b>Rp 21.000.000</b>
                                                          </td>
                                                    </tr>                                                      
                                              </table>
                                        </div>  
                                    </div>

                                    <div class="payment__step__left--shipping">
                                          <!--*************** Shipping details ***************-->
                                          <div class="payment__step__box">
                                                <form action="">
                                                      <!-- name -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>Name</b></label>
                                                            <input class="form__content--input" type="text"><br>
                                                      </div>    
                                                      <!-- Address -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>Shipping Address</b></label>
                                                            <textarea class="form__content--textarea" name="" id="" rows="4"></textarea><br>
                                                      </div>                                          
                                                      <!-- Province -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>Province</b></label>
                                                            <select class="combobox form__content--select">
                                                                  <option class="" value="volvo" selected>Mr</option>
                                                                  <option class="" value="saab">Mrs</option>
                                                            </select><br>
                                                      </div>                                                      
                                                      <!-- City -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>City</b></label>
                                                            <select class="combobox form__content--select">
                                                                  <option class="" value="volvo" selected>Mr</option>
                                                                  <option class="" value="saab">Mrs</option>
                                                            </select><br>
                                                      </div>                                                      
                                                      <!-- Area -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>Area</b></label>
                                                            <select class="combobox form__content--select">
                                                                  <option class="" value="volvo" selected>Mr</option>
                                                                  <option class="" value="saab">Mrs</option>
                                                            </select>
                                                            <span class="form__content--info info--failed">*This is a required field</span><br>
                                                      </div>                                                      
                                                      <!-- Zip/Postal Code -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                            <label for="" class="form__content--label"><b>Zip/Postal Code</b></label>
                                                            <input class="form__content--input" type="text"><br>
                                                      </div>
                                                      <!-- Phone Number -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                      <label for="" class="form__content--label"><b>Phone Number</b></label>
                                                      <input class="form__content--input" type="text"><br>
                                                      </div>
                                                      <!-- Phone Number -->
                                                      <div class="form__content--divwrapper form__content--divwrapper--shipping">
                                                      <label for="" class="form__content--label form__content--label--shippingcost"><h3 class="shipping__cost--text">Shipping Cost</h3></label>
                                                      <span class="shipping__cost--price">Rp 500.000</span><br>
                                                      <small class="shipping__cost--price--small">
                                                        * If you cannot find your area, please contact our customer service and 
                                                        we will make your order by phone directly.
                                                      </small>
                                                      </div>                                                      
                                                </form>
                                          </div>
                                    </div>
                                    
                                    <!-- button porcess to payment -->
                                    <br>
                                    <span class="payment__button__box">
                                      <a href="" class="button--account">PROCESS TO PAYMENT</a>   
                                    </span>
                                     
                              </div>    		
                  	</section>
              </div>

              <!-- popup search -->
              <?php include("includes/popup_search.php"); ?>

		</section>

<!-- footer -->
<?php include("includes/footer.php"); ?>

