


<!-- header -->
<?php include("includes/header.php"); ?>




		<!-- main -->
		<section class="main__container__wrapper"> <!-- bg--blur -->
			<center>
                        <img class="molt banner--article" 
                        data-molt-0w="assets/images/img__banner__page/banner--products--320.jpg"
                        data-molt-480w="assets/images/img__banner__page/banner--products--768.jpg"
                        data-molt-768w="assets/images/img__banner__page/banner--products--1360.jpg" 
                        data-molt-1400w="assets/images/img__banner__page/banner--products--1920.jpg" alt="">
                  </center>
                  <div class="main__container__3">
                  	<p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> Products</a></p>
                        <hr class="line--hr" style="border-top:1px solid #e3e3e3;">

                        <!-- Sidebar Left -->
                  	<?php include("includes/products--sidebar_left.php"); ?>
                 
                        <!-- product wrapper -->
                  	<section class="product__wrapper">
                              <div class="product__sorting">
                                    <label for="" class="product__sorting--label"> Sort by :</label>
                                    <select class="combobox product__sorting--select">
                                          <option class="product__sorting--option" value="volvo" selected>All</option>
                                          <option class="product__sorting--option" value="saab">New Arrivals</option>
                                          <option class="product__sorting--option" value="vw">Price : Low-high</option>
                                          <option class="product__sorting--option" value="audi">Price : High-low</option>
                                    </select>
                              </div>
                              <ul class="product--ul" id="filter">                                   
                              </ul>


                              <?php include("includes/modal--filter.php"); ?>


                              <hr class="line--hr" style="border-top:1px solid #e3e3e3;">

                              <!-- pagination -->
                              <div class="product__pagination">
                                    <ul class="product__pagination--ul">
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a"><span class="icon--pagination icon--pagination--left"></span></a></li>
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a product__pagination--active">1</a></li>
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a">2</a></li>
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a">3</a></li>
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a">4</a></li>
                                          <li class="product__pagination--li"><a href="" class="product__pagination--a"><span class="icon--pagination icon--pagination--right"></span></a></li>
                                    </ul>
                              </div>        		
                  	</section>
                  </div>

		</section>

<!-- footer -->
<?php include("includes/footer.php"); ?>

