
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper main__container__wrapper--contact">
            <div class="main__container__2">
                <p class="breadscrumbs"><a href="" class="breadscrumbs--a">Home </a> / <a href="" class="breadscrumbs--a"> Contact Us</a></p>
                <div class="main__container--map">
                    <!-- js maps -->
                    <script src="https://maps.googleapis.com/maps/api/js"></script>
                    <div id="map_canvas"></div>
                </div>
                <div class="main__container--desc">
                    <article class="desc--article">                        
                        <h1>Contact Us</h1>
                        <p>Should you need to express something to your beloved, we welcome you to visit and free consult with us for solution!</p>
                            <h4>FLOWER STUDIO - Flower Shop</h4>    
                            <p style="margin:0;">Midplaza 1, Basement 1<br>
                            Jl. Jendral Sudirman Kav. 10-11 Jakarta 12210<br>
                            <span class="icon--article logo--phone"></span> : <b>+62 21 570 3666</b><br>
                            <span class="icon--article logo--email"></span> : <a href="mailto:florist@flowerstudio.co.id"><i>florist@flowerstudio.co.id</i></a></p>
                        </p>
                    </article>              
                </div>
            </div>            
		</section>

<?php include("includes/footer.php"); ?>

