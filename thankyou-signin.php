
<!-- header -->
<?php include("includes/header.php"); ?>

    <!-- main -->
    <section class="main__container__wrapper"> <!-- bg--blur -->
              <div class="main__container__2">
                                                
                    <!-- shipping wrapper -->
                    <section class="account__wrapper"> 
                              <div class="payment__step--main">
                                    
                                    <!-- success order payment -->
                                    <span class="payment__success">
                                        <h2 class="">THANK YOU FOR SIGNING UP !</h2>
                                        <hr class="payment__success--hr">
                                        <p class="payment__success--p">
                                          Check your email to complete registration.<br>
                                          Didn't get the email ? Please <a href="">click here</a> to resend it.
                                        </p>                                        
                                    </span>
                                     
                              </div>        
                    </section>
              </div>

              <!-- popup search -->
              <?php include("includes/popup_search.php"); ?>

    </section>

