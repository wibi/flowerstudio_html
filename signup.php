
<!-- header -->
<?php include("includes/header.php"); ?>

		<!-- main -->
		<section class="main__container__wrapper main__container--signup"> <!-- bg--blur -->
                  <div class="main__container__2">
                        <br>
                        <div class="main__signup--img"></div>
                        <div class="main__signup--box">
                              <h2 class="main__signup--h2">SIGN UP</h2>
                              <span class="info__alert alert--failed"><span class="icon--alert--failed"></span>please complete all the information fields.</span>
                              <form class="" action="">
                                    <div class="input__side">
                                          <div class="input__side__left">
                                                <label for="" class="signup--label"> First Name:</label><br>
                                                <span class="signup--span">
                                                      <select class="combobox sign--select">
                                                            <option class="" value="volvo" selected>Mr</option>
                                                            <option class="" value="saab">Mrs</option>
                                                      </select>
                                                      <input class="sign__input--select" type="text"> 
                                                </span>
                                                <br>
                                                <label for="" class="signup--label">Email</label><br>
                                                <input class="signup--input" type="text"><br>
                                                <label for="" class="signup--label">Password</label><br>
                                                <input class="signup--input" type="text"><br>
                                          </div>
                                          <div class="input__side__right">
                                                <label for="" class="signup--label">Last Name</label><br>
                                                <input class="signup--input" type="text"><br>
                                                <label for="" class="signup--label">Phone Number</label><br>
                                                <input class="signup--input" type="text"><br>
                                                <label for="" class="signup--label">Password Confirm</label><br>
                                                <input class="signup--input" type="text"><br>
                                          </div>
                                    </div>
                                    <a href="" class="button button--signup">SUBMIT</a><font class="info">*) ACCEPT AND AGREE TO ALL OF ITS TERMS AND CONDITION</font>
                              </form>
                        </div> 
                  </div>               
                  <span class="bg--shadow"></span>                 
		</section>            

<!-- footer -->
<?php include("includes/footer.php"); ?>

